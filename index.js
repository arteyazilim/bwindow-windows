(function(root, factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['qw', 'Bwindow'], factory);
    } else if (typeof module === 'object' && typeof module.exports === 'object') {
        factory(require('qw'), require('Bwindow'));
    } else {
        factory(root.qw, root.Bwindow);
    }
})(this, function(qw, Bwindow) {
    'use strict';

    var labels = {
        okLabel: 'Tamam',
        yesLabel: 'Evet',
        noLabel: 'Hayır',
        cancelLabel: 'Vazgeç',
        closeLabel: 'Kapat',
        refreshLabel: 'Yenile',
        resetLabel: 'Sıfırla'
    };

    if (window.bwindowLabels && qw.isPlainObject(window.bwindowLabels)) {
        qw.extend(false, labels, window.bwindowLabels);
    }

    // alert
    Bwindow.alert = function(title, content)
    {
        return Bwindow(content, {
            title: title,
            position: ['center', 100],
            extraClass: 'b-alert'
        });
    };

    // prompt
    Bwindow.prompt = function(title, label, options)
    {
        var def = {
            description : '',
            defaultValue : '',
            okFn : function(){},
            cancelFn : function(win){win.close();},
            okLabel : labels.okLabel,
            cancelLabel : labels.cancelLabel,
            inputOnKeypress: false,
            inputOnKeydown: false,
            inputOnKeyup: false,
            inputWidth: '220px'
        };

        if (options && qw.isPlainObject(options)) {
            qw.extend(false, def, options);
        }

        var container = qw.create('div'),
            labelElement = qw.create('label').appendTo(container),
            inputContainer = qw.create('span'),
            inputElement = qw.create('input');

        if (label) {
            qw.create('span').html(label).appendTo(labelElement);
        }

        inputContainer.appendTo(labelElement);
        inputElement
            .prop('type', 'text')
            .prop('value', def.defaultValue)
            .css('width', def.inputWidth)
            .appendTo(inputContainer);

        if (def.description && qw.isString(def.description)) {
            qw.create('p')
                .html(def.description)
                .appendTo(container);
        }

        var bwin = Bwindow(container, {
            title: title,
            position: ['center', 100],
            extraClass: 'b-prompt',
            events: {
                after_show: function()
                {
                    var input = this.getBody().find('input');
                    input[0].focus();
                    input[0].select();
                }
            },
            buttons: {
                toolbar: [],
                right: [
                    {
                        html: def.okLabel,
                        fn: function(event, win)
                        {
                            var input = win.getBody().find('input');
                            if (qw.trim(input[0].value) === '') {
                                input[0].focus();
                            } else if (qw.isFunction(def.okFn)) {
                                def.okFn(win, input[0].value);
                            }
                        }
                    },
                    {
                        html: def.cancelLabel,
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.cancelFn)) {
                                def.cancelFn(win);
                            }
                        }
                    }
                ]
            }
        });

        inputElement.on('keypress', function(ev)
        {
            if (ev.keyCode === 13 && qw.trim(this.value) !== '') {
                if (qw.isFunction(def.okFn)) {
                    def.okFn(bwin, this.value);
                }
            } else if (qw.isFunction(def.inputOnKeypress)) {
                def.inputOnKeypress.call(this, ev);
            }
        });
        if (qw.isFunction(def.inputOnKeydown)) {
            inputElement.on('keydown', function(ev) {
                def.inputOnKeydown.call(this, ev);
            });
        }
        if (qw.isFunction(def.inputOnKeyup)) {
            inputElement.on('keydown', function(ev) {
                def.inputOnKeyup.call(this, ev);
            });
        }
        return bwin;
    };

    // yesNo
    Bwindow.yesNo = function(title, content, options)
    {
        var def = {
            yesFn : function(){},
            noFn : function(win){win.close();},
            yesLabel : labels.yesLabel,
            noLabel : labels.noLabel
        };

        if (options && qw.isPlainObject(options)) {
            qw.extend(false, def, options);
        }

        return Bwindow(content, {
            title: title,
            position: ['center', 100],
            extraClass: 'b-yes-no',
            buttons: {
                toolbar: [],
                right: [
                    {
                        html: def.yesLabel,
                        type: 'button',
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.yesFn)) {
                                def.yesFn(win);
                            }
                        }
                    },
                    {
                        html: def.noLabel,
                        type: 'button',
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.noFn)) {
                                def.noFn(win);
                            }
                        }
                    }
                ]
            }
        });
    };

    // yesCancel
    Bwindow.yesCancel = function(title, content, options)
    {
        var def = {
            yesFn : function(){},
            cancelFn : function(win){win.close();},
            yesLabel : labels.yesLabel,
            cancelLabel : labels.cancelLabel
        };

        if (options && qw.isPlainObject(options)) {
            qw.extend(false, def, options);
        }

        return Bwindow(content, {
            title: title,
            position: ['center', 100],
            extraClass: 'b-yes-cancel',
            buttons: {
                toolbar: [],
                right: [
                    {
                        html: def.yesLabel,
                        type: 'button',
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.yesFn)) {
                                def.yesFn(win);
                            }
                        }
                    },
                    {
                        html: def.cancelLabel,
                        type: 'button',
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.cancelFn)) {
                                def.cancelFn(win);
                            }
                        }
                    }
                ]
            }
        });
    };

    // yesNoCancel
    Bwindow.yesNoCancel = function(title, content, options)
    {
        var def = {
            yesFn : function(){},
            noFn : function(){},
            cancelFn : function(win){win.close();},
            yesLabel : labels.yesLabel,
            noLabel : labels.noLabel,
            cancelLabel : labels.cancelLabel
        };

        if (options && qw.isPlainObject(options)) {
            qw.extend(false, def, options);
        }

        return Bwindow(content, {
            title: title,
            position: ['center', 100],
            extraClass: 'b-yes-no-cancel',
            buttons: {
                toolbar: [],
                left: [
                    {
                        html: def.cancelLabel,
                        type: 'button',
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.cancelFn)) {
                                def.cancelFn(win);
                            }
                        }
                    }
                ],
                right: [
                    {
                        html: def.yesLabel,
                        type: 'button',
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.yesFn)) {
                                def.yesFn(win);
                            }
                        }
                    },
                    {
                        html: def.noLabel,
                        type: 'button',
                        fn: function(event, win)
                        {
                            if (qw.isFunction(def.noFn)) {
                                def.noFn(win);
                            }
                        }
                    }
                ]
            }
        });
    };

    // iframe
    Bwindow.iframe = function(title, url, width, height, maximized)
    {
        return Bwindow('<iframe src="' + url + '" style="border:0;margin:0;padding:0;display:block;width:100%;height:100%;" border="0" frameborder="0"></iframe>',
            {
                title: title,
                maximized: !!maximized,
                extraClass: 'b-iframe',
                size: {
                    width: width,
                    height: height,
                    type: 'content'
                },
                buttons: {
                    toolbar: [
                        {
                            html: labels.resetLabel,
                            type: 'button',
                            fn: function(event, win)
                            {
                                win.resizeTo(width, height, 'content');
                                var iframe = win.getBody(true).getElementsByTagName('iframe')[0];
                                iframe.src = url + '';
                            }
                        },
                        {
                            html: labels.closeLabel,
                            type: 'button',
                            fn: function(event, win)
                            {
                                var body = win.getBody(true),
                                    iframe = body.getElementsByTagName('iframe')[0];
                                body.removeChild(iframe);
                                win.close();
                            }
                        }
                    ]
                }
            }
        );
    };

    // image
    Bwindow.image = function(url, title, options)
    {
        var def = {
            buttons: null,
            extraClass: 'b-image',
            overlayClose: true,
            draggable: false,
            size: {
                type: 'content'
            },
            minSize: {
                width: 10,
                height: 10,
                type: 'container'
            }
        };

        if (options && qw.isPlainObject(options)) {
            qw.extend(false, def, options);
        }

        if (!qw.isString(title) || qw.trim(title) == '') {
            title = null;
        }
        var bwin = Bwindow('<div class="loading"></div>', def);
        var image = qw.create('img')
            .css({
                position: 'absolute',
                left: '-9999px'
            })
            .on('load', {win: bwin}, function(event) {
                var win = event.data.win;
                if (!win.isClosed()) {
                    this.style.position = '';
                    this.style.left = '';
                    win.empty()
                        .resizeTo(this.width, this.height, 'content')
                        .setContent(this);
                    if (title) {
                        win.addContent('<h3 class="bwindow-image-title' + (this.width < 500 ? ' small-image' : '') + '">' + title + '</h3>');
                    }
                    win.fixScroll();
                }
            })
            .appendTo(document.body)
            .attr('src', url);
        return bwin;
    }
});
