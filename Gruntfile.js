module.exports = function(grunt) {
    grunt.initConfig({
        uglify: {
            main: {
                files: {
                    'dist/bwindow.windows.min.js': ['index.js']
                }
            }
        },
        cssmin: {
            main: {
                files: {
                    'dist/bwindow.windows.min.css': ['bwindow.windows.css']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('addlinefeed', 'Add Line Feed', function() {
        var fs = require('fs');
        fs.appendFileSync('dist/bwindow.windows.min.js', "\n");
        fs.appendFileSync('dist/bwindow.windows.min.css', "\n");
    });

    grunt.registerTask('default', 'Default Tasks', function() {
        grunt.task.run(['uglify', 'cssmin', 'addlinefeed']);
    });
};
